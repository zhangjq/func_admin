export default [
  {
    path: '/',
    redirect: '/welcome',
  },


  {
    name: '配置管理',
    icon: 'tool',
    path: '/config',
    authority: ['admin', 'user'],
    routes: [
      {
        path: '/config/productlist',
        name: '产品管理',
        component: './config/index',
      },
      {
        path: '/config/productconfig/detail',
        name: '配置表详情',
        icon: 'shopping',
        hideInMenu: true,
        component: './config/ConfigDetail',
      },
    ],
  },
  
  {
    name: '用户中心',
    icon: 'user',
    path: '/user',
    authority: ['admin', 'user'],
    routes: [
      {
        path: '/user/account',
        name: '账户管理',
        component: './user/account/index',
      },
      {
        path: '/user/order',
        name: '订单管理',
        component: './user/order/index',
      },
    ],
  },

  {
    path: '/admin',
    name: '竞猜配置',
    icon: 'lineChart',
    // component: './Admin',
    authority: ['admin'],
    // new page
  },

  {
    path: '/account',
    name: '账号管理',
    icon: 'team',
    authority: ['admin', 'user'],
    routes: [
      {
        path: '/account/list',
        name: '账号查询',
        icon: 'table',
        component: './account/list/index',
        authority: ['admin', 'user'],
      },
    ],
  },

];
