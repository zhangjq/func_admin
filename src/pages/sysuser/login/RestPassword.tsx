import { Form, Input, Button } from 'antd';
import React from 'react';

import LoginFrom from './components/Login';

import styles from './style.less';

const { UserName, Password, Email, Captcha, Submit } = LoginFrom;

const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

const Restpassword = () => {
  const onFinish = values => {
    console.log('Success:', values);
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  return (
    <div className={styles.main} style={{ width: 660, position: 'relative', left: -60 }}>
      <Form
        {...layout}
        name="basic"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          label="用户名"
          name="username"
        >
          <span className="ant-form-text">username</span>
        </Form.Item>

        <Password
          name="oldpassword"
          label="原密码"
          placeholder="原密码"
          rules={[
            {
              required: true,
              message: '请输入原密码！',
            },
          ]}
        />

        <Password
          name="password"
          label="新密码"
          placeholder="新密码"
          rules={[
            {
              required: true,
              message: '请输入新密码！',
            },
          ]}
        />

        <Password
          name="confirmpassword"
          dependencies={['password']}
          label="确认新密码"
          placeholder="确认新密码"
          rules={[
            {
              required: true,
              message: '请确认新密码！',
            },
            ({ getFieldValue }) => ({
              validator(rule, value) {
                if (!value || getFieldValue('password') === value) {
                  return Promise.resolve();
                }
                // eslint-disable-next-line prefer-promise-reject-errors
                return Promise.reject('两次密码不一致!');
              },
            }),
          ]}
        />

        <Captcha
          name="captcha"
          label="邮件验证码"
          placeholder="邮件验证码"
          countDown={120}
          getCaptchaButtonText=""
          getCaptchaSecondText="秒"
          rules={[
            {
              required: true,
              message: '请输入邮件验证码！',
            },
          ]}
        />

        <Form.Item {...tailLayout}>
          {/* <Button type="primary" htmlType="submit"> 提交 </Button> */}
          <Submit loading={false} {...tailLayout}>提交</Submit> 
        </Form.Item>
      </Form>
    </div>
  );
};

export default Restpassword;
