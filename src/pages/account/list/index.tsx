import React from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';

import AccountList from './AccountList';

export default () => (
  <PageHeaderWrapper>
    <AccountList />
  </PageHeaderWrapper>
)
