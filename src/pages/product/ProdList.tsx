import React, { useState } from 'react';
import Link from 'umi/link';
import { Tag, Button, Avatar } from 'antd';
import ProTable, { ProColumns } from '@ant-design/pro-table';
import { PlusOutlined } from '@ant-design/icons';
import ProductNew from './ProductNew';
// import router from 'umi/router';

const columns: ProColumns<{}>[] = [
  {
    title: 'icon',
    dataIndex: 'icon',
    key: 'icon',
    render: () => (
      <Avatar shape="square" size={52} 
        src="https://gw.alipayobjects.com/zos/rmsportal/dURIMkkrRFpPgTuzkwnB.png" />
    ),
  },
  {
    title: 'appId',
    dataIndex: 'appId',
    key: 'appId',
    render: (text: React.ReactNode) => <a>{text}</a>,
  },
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
    render: (text: React.ReactNode) => <a>{text}</a>,
  },
  {
    title: 'Version',
    dataIndex: 'Version',
    key: 'Version',
  },
  {
    title: 'description',
    dataIndex: 'description',
    key: 'description',
  },
  {
    title: 'Tags',
    key: 'tags',
    dataIndex: 'tags',
    render: (tags: any[]) => (
      <span>
        {tags.map(tag => {
          let color = tag.length > 5 ? 'geekblue' : 'green';
          if (tag === 'loser') {
            color = 'volcano';
          }
          return (
            <Tag color={color} key={tag}>
              {tag.toUpperCase()}
            </Tag>
          );
        })}
      </span>
    ),
  },
  {
    title: 'Action',
    key: 'action',
    render: () => (
      <span>
        <Link to="/config/productconfig">配置</Link>
        <a>控制台</a>
        <a>成员</a>
        {/* <a style={{ marginLeft: 16 }}>性能</a> */}
        <a>维护</a>
        <a>下线</a>
      </span>
    ),
  },
];

const data = [
  {
    appId: '1',
    name: '赛事1',
    Version: '1.01',
    description: 'New York No. 1 Lake Park',
    member: [],
    tags: ['nice', 'developer'],
  },
  {
    appId: '2',
    name: '赛事2',
    Version: '4.20',
    description: 'London No. 1 Lake Park',
    tags: ['loser'],
  },
  {
    appId: '3',
    name: '捕鱼',
    Version: '1.32',
    description: 'Sidney No. 1 Lake Park',
    tags: ['cool', 'teacher'],
  },
];

// let handleProdNewVisible: (show: boolean) => void;

export default () => {
  const [prodNewVisible, setProdNewVisible] = useState(false);
  return [
    <ProTable<{}, {}> 
      size="large"
      headerTitle="产品查询"
      columns={columns} dataSource={data}
      toolBarRender={() => [
        <Button type="primary" onClick={() => setProdNewVisible(true)}>
            <PlusOutlined /> 添加产品
        </Button>,
      ]}
    />,
    <ProductNew
      modalVisible={prodNewVisible}
      onCancel={() => setProdNewVisible(false)}/>
  ];
}
