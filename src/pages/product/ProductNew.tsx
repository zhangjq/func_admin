import React from 'react';
import { Modal } from 'antd';
import ProTable, { ProColumns } from '@ant-design/pro-table';
// import { TableListItem } from '../ListTableList/data';

const columns: ProColumns<TableListItem>[] = [
  {
    title: '规则名称',
    dataIndex: 'name',
    rules: [
      {
        required: true,
        message: '规则名称为必填项',
      },
    ],
  },
  {
    title: '描述',
    dataIndex: 'desc',
    valueType: 'textarea',
  },
  {
    title: '服务调用次数',
    dataIndex: 'callNo',
    sorter: true,
    hideInForm: true,
    renderText: (val: string) => `${val} 万`,
  },
  {
    title: '状态',
    dataIndex: 'status',
    hideInForm: true,
    valueEnum: {
      0: { text: '关闭', status: 'Default' },
      1: { text: '运行中', status: 'Processing' },
      2: { text: '已上线', status: 'Success' },
      3: { text: '异常', status: 'Error' },
    },
  },
  {
    title: '上次调度时间',
    dataIndex: 'updatedAt',
    sorter: true,
    valueType: 'dateTime',
    hideInForm: true,
  },
  {
    title: '操作',
    dataIndex: 'option',
    valueType: 'option',
    render: (_, record) => (
      <>
        <a
          onClick={() => {
            handleUpdateModalVisible(true);
            setStepFormValues(record);
          }}
        >
          配置
        </a>
        <Divider type="vertical" />
        <a href="">订阅警报</a>
      </>
    ),
  },
];

interface CreateFormProps {
  modalVisible: boolean;
  onCancel: () => void;
}

const ProductNew: React.FC<CreateFormProps> = props => {
  const { modalVisible, onCancel } = props;

  return (
    <Modal
      destroyOnClose
      title="新增产品"
      visible={modalVisible}
      onCancel={() => onCancel()}
      footer={null}
    >
      <ProTable
          onSubmit={async value => {

            // const success = await handleAdd(value);
            // if (success) {
            //   handleModalVisible(false);
            //   if (actionRef.current) {
            //     actionRef.current.reload();
            //   }
            // }
          }}
          rowKey="key"
          type="form"
          columns={columns}
          rowSelection={{}}
        />
    </Modal>
  );
};

export default ProductNew;
