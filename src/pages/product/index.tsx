import React from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';

import ProdList from './ProdList';

export default () => (
  <PageHeaderWrapper>
    <ProdList/>
  </PageHeaderWrapper>
)
