import React, { useState } from 'react';
import { Modal, Button } from 'antd';
import { ExclamationCircleOutlined, PlusOutlined } from '@ant-design/icons';
import ProTable from '@ant-design/pro-table';
import { Link } from 'umi';

// import ConfigDetailModal from '../ConfigDetail';

const { confirm } = Modal;

let showDetailModal = () => {};

function showDeleteConfirm() {
  confirm({
    title: 'Are you sure delete this task?',
    icon: <ExclamationCircleOutlined />,
    content: 'Some descriptions',
    okText: 'Yes',
    okType: 'danger',
    cancelText: 'No',
    onOk() {
      // console.log('OK');
    },
    onCancel() {
      // console.log('Cancel');
    },
  });
}

const columns = [
  {
    title: 'id',
    dataIndex: 'configId',
    key: 'configId',
    hideInSearch: true,
    hideInTable: true,
    render: (text: React.ReactNode) => <a>{text}</a>,
  },
  {
    title: 'namespace',// table
    dataIndex: 'namespace',
    key: 'namespace',
    render: (text: React.ReactNode) => (
      <Link to={`/config/productconfig/detail?namespace=${text}`}>{text}</Link>
    )
  },
  {
    title: 'key',
    dataIndex: 'key',
    key: 'key',
    hideInSearch: true,
  },
  {
    title: 'desc',
    dataIndex: 'desc',
    key: 'desc',
    hideInSearch: true,
  },
  {
    title: 'Version',
    dataIndex: 'Version',
    key: 'Version',
    hideInSearch: true,
  },
  {
    title: 'Action',
    key: 'action',
    hideInSearch: true,
    render: () => (
      <span>
        <Link to="/config/productconfig/detail">详情</Link>
        {/* <a onClick={showDetailModal}>
          修改
        </a> */}
        <a onClick={showDetailModal}>
          克隆
        </a>
        <a onClick={showDeleteConfirm}>
          删除
        </a>
      </span>
    ),
  },
];

const data = [
  {
    configId: '1',
    namespace: 'user',
    Version: '101',
    desc: '',
    member: [],
    tags: ['nice', 'developer'],
  },
  {
    configId: '2',
    namespace: 'app',
    Version: '420',
    desc: '',
    tags: ['loser'],
  },
  {
    configId: '3',
    namespace: 'play.single',
    Version: '101',
    desc: '',
    tags: ['cool', 'teacher'],
  },
  {
    configId: '4',
    namespace: 'play.multi',
    Version: '101',
    desc: '',
    tags: ['cool', 'teacher'],
  },
];

export default () => {
  const [visibleDetail, setVisibleDetail] = useState(false);
  showDetailModal = () => setVisibleDetail(true);

  return [
    <ProTable<{}, {}> 
      headerTitle="配置查询"
      search={false}
      columns={columns} 
      dataSource={data} 
      toolBarRender={() => [
        <Button type="primary" onClick={() => setVisibleDetail(true)}>
            <PlusOutlined /> 添加配置表
        </Button>,
      ]}
    />,
    // <ConfigDetailModal 
    //   visible={visibleDetail}
    //   setVisible={setVisibleDetail}
    // />
  ];
};
