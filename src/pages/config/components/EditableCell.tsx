import React from 'react';
import { Table, Input, InputNumber, Popconfirm, Form } from 'antd';

interface Item {
  key: string;
  name: string;
  age: number;
  address: string;
}

interface EditableCellProps extends React.HTMLAttributes<HTMLElement> {
  editing: boolean;
  dataIndex?: string;
  title?: any;
  inputType: 'number' | 'text' | 'code';
  record: Item;
  index?: number;
  children: React.ReactNode;
}

const EditableCell: React.FC<EditableCellProps> = ({
  editing,
  dataIndex,
  title,
  inputType,
  record,
  index,
  children,
  ...restProps
}) => {
  const value: any = (children && typeof children.props === 'object') ? children.props.children : children;

  let inputNode;
  switch (inputType) {
    case 'number':
      inputNode = <InputNumber value={value} />;
      break;
    case 'code':
      inputNode = <Input value={value} />;
      break;
    default:
      inputNode = <Input value={value} />;
      break;
  }

  return (
    <td {...restProps}>
      {editing ? (
        <Form.Item
          name={dataIndex}
          style={{ margin: 0 }}
          rules={[
            {
              required: true,
              message: `Please Input ${title}!`,
            },
          ]}
        >
          {inputNode}
        </Form.Item>
      ) : (
        value
      )}
    </td>
  );
};

export default EditableCell;
