import React, { useState } from 'react';
import { Modal, Table } from 'antd';

import SyntaxHighlighter from 'react-syntax-highlighter';
import { docco } from 'react-syntax-highlighter/dist/esm/styles/hljs';
import ProTable from '@ant-design/pro-table';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import EditableCell from './components/EditableCell';

const DetailTable = () => {
  // const [form] = Form.useForm();
  const [editingKey, setEditingKey] = useState('');
  const isEditing = (record: any) => record.key === editingKey;

  const edit = (record: any) => {
    // form.setFieldsValue({ ...record });
    console.log(record);
    setEditingKey(record.key);
  };

  const cancel = () => {
    setEditingKey('');
  };

  const save = async (key: React.Key) => {};

  const dataSource = [
    {
      key: 'name1',
      value: '100',
      desc: 'ssss',
      type: 'number',
      rules: '(x>1&&x<100)||(x==0)',
    },
    {
      key: 'name2',
      value: '20',
      desc: 'ssss',
      type: 'email',
      rules: '(x>1&&x<100)||(x==0)',
      // rules: 'testReg(/\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*/)',
    },
  ];

  const columns = [
    {
      title: 'key',
      dataIndex: 'key',
      key: 'key',
      width: 50,
      editable: false,
    },
    {
      title: 'value',
      dataIndex: 'value',
      key: 'value',
      editable: true,
    },
    {
      title: 'desc',
      dataIndex: 'desc',
      key: 'desc',
      editable: true,
      ellipsis: true,
    },
    {
      title: 'type',
      dataIndex: 'type',
      key: 'type',
      width: 100,
      editable: true,
    },
    {
      title: 'rules',
      dataIndex: 'rules',
      key: 'rules',
      editable: true,
      width: 350,
      render: (text: React.ReactNode) => (
        <SyntaxHighlighter language="javascript" style={docco}>
          {text}
        </SyntaxHighlighter>
      ),
    },
    {
      title: 'Action',
      key: 'action',
      render: (_: any, record: any) => (
        <span>
          <a onClick={() => edit(record)}>修改</a>
          &nbsp;&nbsp;
          <a onClick={() => cancel()}>取消</a>
        </span>
      ),
    },
  ];

  const mergedColumns = columns.map(col => {
    if (!col.editable) {
      return col;
    }
    const renderTemp = col.render;
    return {
      ...col,
      render: (text: React.ReactNode, record: any) => (
        <EditableCell
          record={record}
          inputType={col.dataIndex === 'rules' ? 'code' : 'text'}
          editing={isEditing(record)}
        >
          {renderTemp ? renderTemp(text) : text}
        </EditableCell>
      ),
    };
  });

  return (
    <ProTable
      search={false}
      dataSource={dataSource}
      columns={mergedColumns}
      rowClassName="editable-row"
      pagination={false}
    />
  );
};

// const ConfigDetailModal = (props: { visible: any; setVisible: (arg0: boolean) => any }) => (
//   <Modal
//     width={980}
//     visible={props.visible}
//     onOk={() => props.setVisible(false)}
//     onCancel={() => props.setVisible(false)}
//   >
//     {/* <h2 style={{ position: 'absolute', zIndex: 3 }}>namespace配置表</h2> */}
//     <ConfigTable />
//   </Modal>
// );

const ConfigDetail = () => (
  <PageHeaderWrapper
    title="配置表详情">
    <DetailTable />
  </PageHeaderWrapper>
);

export default ConfigDetail;
