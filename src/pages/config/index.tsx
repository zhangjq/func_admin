import React from 'react';

import { PageHeaderWrapper } from '@ant-design/pro-layout';
import ConfigTable from './components/ConfigTable';

export default () => (
  <PageHeaderWrapper
    title="赛事1 - 配置表">
    <ConfigTable />
  </PageHeaderWrapper>
);
