import React, { useState } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';

import { Menu } from 'antd';

import BetOrder from './BetOrder';
import TakeoutOrder from './TakeoutOrder';
import Recharge from './Recharge';


const Order: React.FC = () => {
  const [current, setCurrent] = useState('bet');
  const [content, setContent] = useState(<BetOrder/>);

  const handleClick = (e: any) => {
    switch(e.key) {
      case 'bet':
        setContent(<BetOrder/>);
        break;
      case 'takeout': 
        setContent(<TakeoutOrder/>);
        break;
      case 'recharge': 
        setContent(<Recharge/>);
        break;
      default:
        setContent(<></>);
        break;
    }
    setCurrent(e.key);
  };

  return (
    <>
      <Menu onClick={handleClick} selectedKeys={[current]} mode="horizontal">
        <Menu.Item key="bet">
          投币订单
        </Menu.Item>
        <Menu.Item key="takeout">
          提币订单
        </Menu.Item>
        <Menu.Item key="recharge">
          充值订单
        </Menu.Item>
      </Menu>
      <br/>
      {content}
    </>
  );
};

export default Order;
