import React from 'react';
import { Tag, Avatar } from 'antd';
import ProTable, { ProColumns } from '@ant-design/pro-table';

// import router from 'umi/router';
// import Link from 'umi/link';

const columns: ProColumns<{}>[] = [
  {
    title: '头像',
    dataIndex: 'icon',
    key: 'icon',
    hideInSearch: true,
    render: () => (
      <Avatar
        shape="square"
        src="https://gw.alipayobjects.com/zos/rmsportal/dURIMkkrRFpPgTuzkwnB.png"
      />
    ),
  },
  {
    title: 'userId',
    dataIndex: 'userId',
    key: 'userId',
    render: (text: React.ReactNode) => <a>{text}</a>,
  },
  {
    title: '用户名',
    dataIndex: 'name',
    key: 'name',
    render: (text: React.ReactNode) => <a>{text}</a>,
  },
  {
    title: 'Email',
    dataIndex: 'email',
    key: 'email',
    // render: (text: React.ReactNode) => <a>{text}</a>,
  },
  {
    title: 'Moblie',
    dataIndex: 'moblie',
    key: 'moblie',
    // render: (text: React.ReactNode) => <a>{text}</a>,
  },
  {
    title: '状态',
    key: 'status',
    dataIndex: 'status',
    render: (tags: any[]) => (
      <span>
        {tags.map(tag => {
          let color = tag.length > 5 ? 'geekblue' : 'green';
          if (tag === 'forbidden') {
            color = 'volcano';
          }
          return (
            <Tag color={color} key={tag}>
              {tag.toUpperCase()}
            </Tag>
          );
        })}
      </span>
    ),
  },
];

const data = [
  {
    userId: '1',
    name: '张三',
    moblie: '+861367890554',
    email: 'junqing258@gamil.com',
    status: ['active'],
  },
  {
    appId: '2',
    name: '张三',
    moblie: '+861367890554',
    email: 'junqing258@gamil.com',
    status: ['forbidden'],
  },
];

const Table: React.FC = () => (
  // const [prodNewVisible, setProdNewVisible] = useState(false);
    <ProTable<{}, {}>
      headerTitle="用户查询"
      columns={columns}
      dataSource={data}
      toolBarRender={false}
    />
  )

export default Table;
