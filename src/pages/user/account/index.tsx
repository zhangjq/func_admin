import React from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import UserTable from './AccountList';


export default () => (
  <PageHeaderWrapper>
    <UserTable/>
  </PageHeaderWrapper>
)
