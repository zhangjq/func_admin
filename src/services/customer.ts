import request from '@/utils/request';


/**
 * account
 */

export async function queryCustomer(): Promise<any> {
  return request('/api/customer');
}

/**
 * order
 */
export async function queryBetOrder(params: any): Promise<any> {
  console.log('===queryBetOrder', params);
  return request('/api/betorder', { data: params });
}