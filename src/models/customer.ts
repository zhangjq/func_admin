
// import { Effect } from 'dva';
// import { Reducer } from 'redux';

export interface CustomerModelState {}


export interface CustomerModelType {
  namespace: 'customer';
  state: CustomerModelState;
  effects: {
    // fetch: Effect;
    // fetchCurrent: Effect;
  };
  reducers: {
    // saveCurrentUser: Reducer<UserModelState>;
    // changeNotifyCount: Reducer<UserModelState>;
  };
}

const CustomerModel: CustomerModelType = {
  namespace: 'customer',

  state: {
    currentCustomer: {},
  },

  effects: {

  },
  reducers: {

  }
};
export default CustomerModel;