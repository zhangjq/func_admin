import { Effect } from 'dva';
import { Reducer } from 'redux';

const queryProduct = () => {};
const queryCurrent = () => {};

export interface ProductType {
  appId?: string;
  appName?: string;
  channleId?: string;
  platfrom?: 'web' | 'ios' | 'android';
}

export interface ProductConfig {
  tableId: number;
  key: string;
  namespace: string;
  dataType: 1 | 2 | 3;
}

export enum ProductConfigValType {
  'option',
  'int',
  'float',
  'tel',
  'text',
  'association'
}

export interface ProductModelState {
  currentProduct?: ProductType;
}

export interface ProductModelType {
  namespace: 'product';
  state: ProductModelState;
  effects: {
    fetch: Effect;
    fetchCurrent: Effect;
  };
  reducers: {
    saveCurrentProduct: Reducer<ProductModelState>;
    changeNotifyCount: Reducer<ProductModelState>;
  };
}

const ProductModel: ProductModelType = {
  namespace: 'product',

  state: {
    currentProduct: {},
  },

  effects: {
    *fetch(_, { call, put }) {
      const response = yield call(queryProduct);
      yield put({
        type: 'save',
        payload: response,
      });
    },
    *fetchCurrent(_, { call, put }) {
      const response = yield call(queryCurrent);
      yield put({
        type: 'saveCurrentUser',
        payload: response,
      });
    },
  },

  reducers: {
    saveCurrentProduct(state, action) {
      return {
        ...state,
        currentUser: action.payload || {},
      };
    },
    changeNotifyCount(
      state = {
        currentProduct: {},
      },
      action,
    ) {
      return {
        ...state,
        currentUser: {
          ...state.currentProduct,
          notifyCount: action.payload.totalCount,
          unreadCount: action.payload.unreadCount,
        },
      };
    },
  },
};

export default ProductModel;
