import { Request, Response } from 'express';

export default {
  '/api/customer': (req: Request, res: Response) => {
    const list: any[] = [];
    res.send({ status: 'ok', list });
  },
  '/api/betorder': (req: Request, res: Response) => {
    const data: any[] = [
      {
        userId: '1',
        name: '张三',
        moblie: '+861367890554',
        email: 'junqing258@gamil.com',
        status: ['active'],
      },
      {
        userId: '2',
        name: '张三',
        moblie: '+861367890554',
        email: 'junqing258@gamil.com',
        status: ['forbidden'],
      },
    ];
    res.send({ status: 'ok', data });
  },

};